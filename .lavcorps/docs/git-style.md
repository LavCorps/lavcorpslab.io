# Git Style Reference
## Terminology
This document uses terminology defined in [RFC 2119](https://datatracker.ietf.org/doc/html/rfc2119).
Miscellaneous terminology includes...
* `x`
    * Integer, often referred to as a "Version number"
* `y`
    * String or Integer, often referred to as a `Issue Name or ID`

## Branching Discipline
Branches are to be named in the following manner:
* `main` is the default branch name
    * **REQUIRED**, as git repos require a default branch
* `feature/issue-name-or-id` is the format for a branch intended to resolve a `todo` issue
    * **REQUIRED** if development is hosted in-repo
    * **RECOMMENDED** if development is hosted in a fork
* `bugfix/issue-name-or-id` or `patch/issue-name-or-id` is the format for a branch intended to fix or patch a `bug` issue
    * **REQUIRED** if development is hosted in-repo
    * **RECOMMENDED** if development is hosted in a fork
* `archive/version-number` is the format for a branch archiving the `main` branch at a certain point or version number. Tags are preferred, but this format is included for posterity.
    * **RECOMMENDED** if archiving before a repo rebase or similar action, else **MUST NOT** in favor of a `tag`

## Tagging Discipline
Tags are to be created in the following manner, based off the `main` branch:
* **REQUIRED:** The `initial commit` of a repo is `v0.0.0`
    * Most important for imported repositories
* **REQUIRED:** The first full release of a repo is `v1.0.0`
    * Tagging is OPTIONAL for versions prior to `v1.0.0`
* **REQUIRED:** A `bugfix`/`patch` merge results in a new tag with the third version number incremented by `1`
* **REQUIRED:** A `feature` merge results in a new tag with the second version number incremented by `1`, and the third version number reset to `0`
* **REQUIRED:** A milestone completion is represented by a new tag with the first version number incremented by `1`, and the second and third verison number reset to `0`
* **REQUIRED:** Indev and Release Candidates are tagged using `v0.0.0-indev.0` or `v0.0.0-rc.0` IF AND ONLY IF the FIRST LINE OF A COMMIT MESSAGE DOESN'T INCLUDE THE VERSION, else tags **SHALL NOT** be created.

## Commit Message Discipline


## Versioning Scheme
The LavCorps-Line Versioning Scheme is compliant with [Semantic Versioning 2.0.0](https://semver.org/spec/v2.0.0.html)!
There are three numbers to the version number: The first, second, and third.
1. **REQUIRED:** The first number represents *major* version, with breaking API changes or otherwise major changes, commits, or merges
2. **REQUIRED:** The second number represents *minor* version, with API additions or otherwise changes, commits, or merges that do not introduce incompatible functions
3. **REQUIRED:** The third number represents *patch* version, with backwards compatible bug fixes, patches, or otherwise extremely minor changes

There's also an extra format RECOMMENDED for indev and rc versions:
* `x.x.x-y-indev.x` for a development version, with the fourth number incremented by `1` for every indev commit.
    * **REQUIRED:** `indev` stages MUST focus on adding new features and fixing bugs.
* `x.x.x-y-rc.x` for a release candidate, with the fourth number incremented by `1` for every rc commit.
    * **REQUIRED:** `rc` stages MUST focus on polishing features and fixing bugs.
    * **RECOMMENDED:** `rc` stages SHOULD be more stable than indev commits.
* **REQUIRED:** The first three version numbers are taken from the version development starts from, with a relevant version number increment depending on what the commit shall introduce.

## Stability Guarantees
* A `tag` that is not an `rc` or `indev` is GUARANTEED to be a STABLE RELEASE 
* The `main` branch is GUARANTEED to be a STABLE RELEASE
* An `rc` is LIKELY to be a STABLE RELEASE, and may include bugs of ANY magnitude, as well as INCOMPLETE or MISSING FEATURES
* ANYTHING NOT GUARANTEED TO BE STABLE HAS NO GUARANTEE OF STABILITY!

## Special Exceptions
There are a few exceptions to the rules and guidelines mentioned in the rest of this document, and they'll be listed below:
* The LavCorps Git Base is exempt from Commit Message Discipline, using its own Commit Message Discipline to ensure clean merge updates to LavCorps-Line repos.
* Repos in which the LavCorps Line participates in development, as opposed to serving as the owner of the repo, are exempt from this process, and LavCorps-Line developers are encouraged to conform to foreign git styles.
