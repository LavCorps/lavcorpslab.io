# CI/CD Rules

## Pipeline Environment
* Most pipelines run in an Alpine docker, with most scripts executing in (largely POSIX compatible) `/bin/zsh`
    * Pipelines can also run Python or another scripting language as part of a stage run script.
    * Pipelines are also welcome to execute in another docker environment, but please be aware that the Alpine docker is a preferred environment due to the lightweightedness and efficiency of an Alpine environment, especially given the fact that all LavCorps-Line pipelines run on the LavCorps-Line Server Systems

## Pipeline Structure
* See `reference.gitlab-ci.yml` for a reference implementation of a standard pipeline
* Unless running on the final stage, do NOT have multiple jobs to a stage due to issues with caching on parallel jobs
    * Stay tuned for changes to this!
* `prestage.sh` runs before EVERY stage, and is intended to setup the Alpine environment
    * Stay tuned for a custom LavCorps-Line Alpine Docker to help partially remediate the need for a `prestage.sh`!

