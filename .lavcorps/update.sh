#!/bin/zsh
echo "Welcome to the LavCorps Git Base Updater!"
echo "This updater is designed to automagically update your repository's LavCorps Git Base."
echo "--- PLEASE WAIT ---"

OWD=$PWD # Set Original Working Directory (OWD)
LGD="" # Initialize Lowest Git Directory (LGD)

while [ "$PWD" != "/" ]; do # While present working directory is NOT root...
    if [ -d .git ]; then # If present working directory has a .git directory...
        LGD=$PWD # Set the Lowest Git Directory variable to represent this current Present Working Directory
    fi # Finish if statement
    cd .. # Change directory to a level lower in our search for a git repo!
done

if [ ! -d "$LGD" ]; then # If we HAVE NOT found a Lowest Git Directory...
    echo "--- LOWEST GIT DIRECTORY NOT FOUND - TERMINATING ---" # Explain what happened
    echo "Make sure you run this script in a git repo, or in a subdirectory of a git repo!" # Offer advice
    return 1 # Self-terminate, returning an error code of 1
fi # Finish if statement

echo "--- FOUND LOWEST GIT DIRECTORY AT "$LGD" ---"
cd $LGD # Change directory to Lowest Git Directory

if [ "! git ls-remote LavCorps-Git-Base" ]; then # If LavCorps-Git-Base is NOT in the list of git remotes...
    git remote add LavCorps-Git-Base git@gitlab.com:LavCorps-Line/lavGitBase.git # Then add it!
fi

echo "--- FETCHING LAVCORPS GIT BASE... ---"
git fetch LavCorps-Git-Base --tags # Fetch LavCorps-GitBase info...

echo "--- MERGING LAVCORPS GIT BASE... ---"
git merge --allow-unrelated-histories LavCorps-Git-Base/deploy # Merge with current repo

git remote remove LavCorps-Git-Base # Remove LavCorps-Git-Base remote, making sure we dont leave any traces that aren't intentional

cd $OWD # Move back to Original Working Directory

echo "--- LAVCORPS GIT BASE UPDATE COMPLETE ---"
echo "Commit changes and upload at your convenience!"
